package com.android.srtpoc;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.srtpoc.Adapters.VoiceListenerAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION_CODE = 1;
    private SpeechRecognizer speechRecognizer;
    private RelativeLayout rvSpeechlayout;
    private LinearLayout stopListenLayout, recogniseLayout;
    private VoiceAnimationView mVoiceAnimationView;
    private TextView tv_result_text;
    private ImageView iv_start_listen;
    private Button btnStopListening;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void startRecognition() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en");
        speechRecognizer.startListening(intent);
        updateUI(true);
    }

    private void showResults(Bundle results) {
        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

        tv_result_text.setText(matches.get(0));
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(this, "Enable Audio Permission", Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO_PERMISSION_CODE);
        }
    }

    private void init() {

        int[] colors = {
                ContextCompat.getColor(this, R.color.color1),
                ContextCompat.getColor(this, R.color.color2),
                ContextCompat.getColor(this, R.color.color3),
                ContextCompat.getColor(this, R.color.color4)
        };

        int[] heights = {40, 44, 38, 43, 36};
        rvSpeechlayout = (RelativeLayout) findViewById(R.id.lytSpeechSpace);
        recogniseLayout = (LinearLayout) findViewById(R.id.lytafterrecognise);
        stopListenLayout = (LinearLayout) findViewById(R.id.lytStopListening);
        tv_result_text = (TextView) findViewById(R.id.tv_result_text);
        iv_start_listen = (ImageView) findViewById(R.id.iv_mic);
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mVoiceAnimationView = (VoiceAnimationView) findViewById(R.id.speech_view);
        mVoiceAnimationView.setSpeechRecognizer(speechRecognizer);
        mVoiceAnimationView.setRecognitionListener(new VoiceListenerAdapter() {
            @Override
            public void onResults(Bundle results) {
                showResults(results);
            }
        });
        mVoiceAnimationView.play();
        btnStopListening = (Button) findViewById(R.id.btnStopListen);
        iv_start_listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermission();
                } else {
                    startRecognition();
                    mVoiceAnimationView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startRecognition();
                        }
                    }, 50);
                }
            }
        });
        btnStopListening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVoiceAnimationView.stop();
                mVoiceAnimationView.play();
                updateUI(false);
            }
        });
    }

    private void updateUI(boolean listening) {
        if (listening) {
            rvSpeechlayout.setVisibility(View.GONE);
            recogniseLayout.setVisibility(View.VISIBLE);
            stopListenLayout.setVisibility(View.VISIBLE);
        } else {
            rvSpeechlayout.setVisibility(View.VISIBLE);
            recogniseLayout.setVisibility(View.GONE);
            stopListenLayout.setVisibility(View.GONE);
            tv_result_text.setText("Listening...");
        }

    }

    @Override
    protected void onDestroy() {
        if (speechRecognizer != null) {
            speechRecognizer.destroy();
        }
        super.onDestroy();
    }
}
