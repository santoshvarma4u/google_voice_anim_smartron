package com.android.srtpoc;

/**
 * Created by SantoshT on 9/14/2017.
 */

public interface PPSAnim {
    void start();
    void stop();
    void animate();
}
