package com.android.srtpoc;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.android.srtpoc.Animations.IdleState;
import com.android.srtpoc.Animations.LoadingAnimation;
import com.android.srtpoc.Animations.TransformationAnim;
import com.android.srtpoc.Animations.WaveAnim;
import com.android.srtpoc.Models.SpeechView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SantoshT on 9/14/2017.
 */

public class VoiceAnimationView extends View implements RecognitionListener {


    private static final int[] DEFAULT_HEIGHT = {40, 44, 38, 43};
    private int[] DEFAULT_COLORS;

    private static final float MDPI_DENSITY = 2.5f;

    private final List<SpeechView> spvs = new ArrayList<>();
    private Paint paint;
    private PPSAnim ppsAnimation;

    private int radius;
    private int spacing;
    private int rotationRadius;
    private int amplitude;

    private float density;

    private boolean isSpeaking;
    private boolean animating;

    private SpeechRecognizer speechRecognizer;
    private RecognitionListener recognitionListener;
    private int barColor = -1;
    private int[] barColors;
    private int[] barMaxHeights;

    private Context ctx;

    public VoiceAnimationView(Context context) {
        super(context);
        this.ctx = context;

        init();
    }

    public VoiceAnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.ctx = context;

        init();
    }

    public VoiceAnimationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.ctx = context;

        init();
    }

    public void play() {
        startIdleInterpolation();
        animating = true;
    }


    public void stop() {
        if (ppsAnimation != null) {
            ppsAnimation.stop();
            ppsAnimation = null;
        }
        animating = false;
        resetBars();
    }


    public void setColors(int[] colors) {
        if (colors == null) return;

        barColors = new int[4];
        if (colors.length < 4) {
            System.arraycopy(colors, 0, barColors, 0, colors.length);
            for (int i = colors.length; i < 4; i++) {
                barColors[i] = colors[0];
            }
        } else {
            System.arraycopy(colors, 0, barColors, 0, 4);
        }
    }

    public void setBarMaxHeightsInDp(int[] heights) {
        if (heights == null) return;

        barMaxHeights = new int[4];
        if (heights.length < 4) {
            System.arraycopy(heights, 0, barMaxHeights, 0, heights.length);
            for (int i = heights.length; i < 4; i++) {
                barMaxHeights[i] = heights[0];
            }
        } else {
            System.arraycopy(heights, 0, barMaxHeights, 0, 4);
        }
    }

    public void setCircleRadiusInDp(int radius) {
        this.radius = (int) (radius * density);
    }

    public void setSpacingInDp(int spacing) {
        this.spacing = (int) (spacing * density);
    }

    public void setIdleStateAmplitudeInDp(int amplitude) {
        this.amplitude = (int) (amplitude * density);
    }


    public void setRotationRadiusInDp(int radius) {
        this.rotationRadius = (int) (radius * density);
    }

    private void init() {

        DEFAULT_COLORS = new int[]{ContextCompat.getColor(ctx, R.color.color1), ContextCompat.getColor(ctx, R.color.color2), ContextCompat.getColor(ctx, R.color.color3), ContextCompat.getColor(ctx, R.color.color4)};
        setColors(DEFAULT_COLORS);
        setBarMaxHeightsInDp(DEFAULT_HEIGHT);
        setCircleRadiusInDp(4);
        setSpacingInDp(4);
        setIdleStateAmplitudeInDp(4);
        setRotationRadiusInDp(20);

        paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.GRAY);
        density = getResources().getDisplayMetrics().density;
        radius = (int) (5 * density);
        spacing = (int) (11 * density);
        rotationRadius = (int) (25 * density);
        amplitude = (int) (3 * density);

        if (density <= MDPI_DENSITY) {
            amplitude *= 2;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (spvs.isEmpty()) {
            return;
        }

        if (animating) {
            ppsAnimation.animate();
        }

        for (int i = 0; i < spvs.size(); i++) {
            SpeechView bar = spvs.get(i);
            if (barColors != null) {
                paint.setColor(barColors[i]);
            } else if (barColor != -1) {
                paint.setColor(barColor);
            }
            canvas.drawRoundRect(bar.getRect(), radius, radius, paint);
        }

        if (animating) {
            invalidate();
        }
    }

    public void setRecognitionListener(RecognitionListener listener) {
        recognitionListener = listener;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (spvs.isEmpty()) {
            initBars();
        } else if (changed) {
            spvs.clear();
            initBars();
        }
    }


    private void initBars() {
        final List<Integer> heights = initBarHeights();
        int firstCirclePosition = getMeasuredWidth() / 2 -
                2 * spacing -
                4 * radius;
        for (int i = 0; i < 4; i++) {
            int x = firstCirclePosition + (2 * radius + spacing) * i;
            SpeechView bar = new SpeechView(x, getMeasuredHeight() / 2, 2 * radius, heights.get(i), radius);
            spvs.add(bar);
        }
    }

    private List<Integer> initBarHeights() {
        final List<Integer> barHeights = new ArrayList<>();
        if (barMaxHeights == null) {
            for (int i = 0; i < 4; i++) {
                barHeights.add((int) (DEFAULT_HEIGHT[i] * density));
            }
        } else {
            for (int i = 0; i < 4; i++) {
                barHeights.add((int) (barMaxHeights[i] * density));
            }
        }
        return barHeights;
    }

    private void resetBars() {
        for (SpeechView bar : spvs) {
            bar.setX(bar.getStartX());
            bar.setY(bar.getStartY());
            bar.setHeight(radius * 2);
            bar.update();
        }
    }

    private void startIdleInterpolation() {
        ppsAnimation = new IdleState(spvs, amplitude);
        ppsAnimation.start();
    }

    private void startRmsInterpolation() {
        resetBars();
        ppsAnimation = new WaveAnim(spvs);
        ppsAnimation.start();
    }

    private void startTransformInterpolation() {
        resetBars();
        ppsAnimation = new TransformationAnim(spvs, getWidth() / 2, getHeight() / 2, rotationRadius);
        ppsAnimation.start();
        ((TransformationAnim) ppsAnimation).setOnInterpolationFinishedListener(new TransformationAnim.OnInterpolationFinishedListener() {
            @Override
            public void onFinished() {
                startRotateInterpolation();
            }
        });
    }

    private void startRotateInterpolation() {
        ppsAnimation = new LoadingAnimation(spvs, getWidth() / 2, getHeight() / 2);
        ppsAnimation.start();
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        if (recognitionListener != null) {
            recognitionListener.onReadyForSpeech(params);
        }
    }

    @Override
    public void onBeginningOfSpeech() {
        if (recognitionListener != null) {
            recognitionListener.onBeginningOfSpeech();
        }
        isSpeaking = true;
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        if (recognitionListener != null) {
            recognitionListener.onRmsChanged(rmsdB);
        }
        if (ppsAnimation == null || rmsdB < 1f) {
            return;
        }
        if (!(ppsAnimation instanceof WaveAnim) && isSpeaking) {
            startRmsInterpolation();
        }
        if (ppsAnimation instanceof WaveAnim) {
            ((WaveAnim) ppsAnimation).onWaveChanged(rmsdB);
        }
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        if (recognitionListener != null) {
            recognitionListener.onBufferReceived(buffer);
        }
    }

    @Override
    public void onEndOfSpeech() {
        if (recognitionListener != null) {
            recognitionListener.onEndOfSpeech();
        }
        isSpeaking = false;
        startTransformInterpolation();
    }

    @Override
    public void onError(int error) {
        if (recognitionListener != null) {
            recognitionListener.onError(error);
        }
    }

    @Override
    public void onResults(Bundle results) {
        if (recognitionListener != null) {
            recognitionListener.onResults(results);
        }
    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        if (recognitionListener != null) {
            recognitionListener.onPartialResults(partialResults);
        }
    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        if (recognitionListener != null) {
            recognitionListener.onEvent(eventType, params);
        }
    }


    public void setSpeechRecognizer(SpeechRecognizer recognizer) {
        speechRecognizer = recognizer;
        speechRecognizer.setRecognitionListener(this);
    }

}
