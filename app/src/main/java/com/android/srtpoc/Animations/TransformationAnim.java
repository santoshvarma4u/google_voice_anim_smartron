package com.android.srtpoc.Animations;

import android.graphics.Point;

import com.android.srtpoc.Models.SpeechView;
import com.android.srtpoc.PPSAnim;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SantoshT on 9/14/2017.
 */

public class TransformationAnim implements PPSAnim {

    private static final long DURATION = 300;

    private long startTimestamp;
    private boolean isPlaying;


    private OnInterpolationFinishedListener listener;

    private final int radius;
    private final int centerX, centerY;
    private final List<Point> finalPositions = new ArrayList<>();
    private final List<SpeechView> spvs;

    public TransformationAnim(List<SpeechView> spvs, int centerX, int centerY, int radius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.spvs = spvs;
        this.radius = radius;
    }

    @Override
    public void start() {
        isPlaying = true;
        startTimestamp = System.currentTimeMillis();
        initFinalPositions();
    }

    @Override
    public void stop() {
        isPlaying = false;
        if (listener != null) {
            listener.onFinished();
        }
    }

    @Override
    public void animate() {
        if (!isPlaying) return;

        long currTimestamp = System.currentTimeMillis();
        long delta = currTimestamp - startTimestamp;
        if (delta > DURATION) {
            delta = DURATION;
        }

        for (int i = 0; i < spvs.size(); i++) {
            SpeechView bar = spvs.get(i);

            int x = bar.getStartX() + (int) ((finalPositions.get(i).x - bar.getStartX()) * ((float) delta / DURATION));
            int y = bar.getStartY() + (int) ((finalPositions.get(i).y - bar.getStartY()) * ((float) delta / DURATION));

            bar.setX(x);
            bar.setY(y);
            bar.update();
        }


        if (delta == DURATION) {
            stop();
        }
    }

    private void initFinalPositions() {
        Point startPoint = new Point();
        startPoint.x = centerX;
        startPoint.y = centerY - radius;
        for (int i = 0; i < 4; i++) {
            Point point = new Point(startPoint);
            rotate((360d / 4) * i, point);
            finalPositions.add(point);
        }
    }


    private void rotate(double degrees, Point point) {

        double angle = Math.toRadians(degrees);

        int x = centerX + (int) ((point.x - centerX) * Math.cos(angle) -
                (point.y - centerY) * Math.sin(angle));

        int y = centerY + (int) ((point.x - centerX) * Math.sin(angle) +
                (point.y - centerY) * Math.cos(angle));

        point.x = x;
        point.y = y;
    }

    public void setOnInterpolationFinishedListener(OnInterpolationFinishedListener listener) {
        this.listener = listener;
    }

    public interface OnInterpolationFinishedListener {
        void onFinished();
    }
}
