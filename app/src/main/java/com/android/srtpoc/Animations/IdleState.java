package com.android.srtpoc.Animations;

import com.android.srtpoc.Models.SpeechView;
import com.android.srtpoc.PPSAnim;

import java.util.List;

/**
 * Created by SantoshT on 9/14/2017.
 */

public class IdleState implements PPSAnim {

    private long startTimestamp;
    private boolean isPlaying;
    private final int floatingAmplitude;
    private final List<SpeechView> spv;
    public IdleState(List<SpeechView> spv, int floatingAmplitude) {
        this.floatingAmplitude = floatingAmplitude;
        this.spv = spv;
    }

    @Override
    public void start() {
        isPlaying = true;
        startTimestamp = System.currentTimeMillis();
    }

    @Override
    public void stop() {
        isPlaying = false;
    }

    @Override
    public void animate() {
        if (isPlaying) {
            update(spv);
        }
    }

    public void update(List<SpeechView> spv) {

        long currTimestamp = System.currentTimeMillis();
        if (currTimestamp - startTimestamp > 1500) {
            startTimestamp += 1500;
        }
        long delta = currTimestamp - startTimestamp;

        int i = 0;
        for (SpeechView mspv : spv) {
            updateCirclePosition(mspv, delta, i);
            i++;
        }
    }

    private void updateCirclePosition(SpeechView mspv, long delta, int num) {
        float angle = ((float) delta / 1500) * 360f + 120f * num;
        int y = (int) (Math.sin(Math.toRadians(angle)) * floatingAmplitude) + mspv.getStartY();
        mspv.setY(y);
        mspv.update();
    }
}

