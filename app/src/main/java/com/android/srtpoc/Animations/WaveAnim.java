package com.android.srtpoc.Animations;

import com.android.srtpoc.Models.SpeechView;
import com.android.srtpoc.PPSAnim;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SantoshT on 9/14/2017.
 */

public class WaveAnim  implements PPSAnim {

    final private List<SpeechWaveAnimation> spvAnims;


    public WaveAnim(List<SpeechView> spvs) {
        this.spvAnims = new ArrayList<>();
        for (SpeechView bar : spvs) {
            spvAnims.add(new SpeechWaveAnimation(bar));
        }
    }

    @Override
    public void start() {
        for (SpeechWaveAnimation waveAnimation : spvAnims) {
            waveAnimation.start();
        }
    }

    @Override
    public void stop() {
        for (SpeechWaveAnimation waveAnimation : spvAnims) {
            waveAnimation.stop();
        }
    }

    @Override
    public void animate() {
        for (SpeechWaveAnimation waveAnimation : spvAnims) {
            waveAnimation.animate();
        }
    }

    public void onWaveChanged(float rmsDB) {
        for (SpeechWaveAnimation waveAnimation : spvAnims) {
            waveAnimation.onRmsChanged(rmsDB);
        }
    }
}
