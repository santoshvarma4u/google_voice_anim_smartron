package com.android.srtpoc.Animations;

import android.graphics.Point;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.android.srtpoc.Models.SpeechView;
import com.android.srtpoc.PPSAnim;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SantoshT on 9/14/2017.
 */

public class LoadingAnimation implements PPSAnim{

    private static final float ROTATION_DEGREES = 720f;
    private static final float ACCELERATION_ROTATION_DEGREES = 40f;

    private long startTimestamp;
    private boolean isPlaying;

    private final int centerX, centerY;
    private final List<Point> startPositions;
    private final List<SpeechView> spvs;

    public LoadingAnimation(List<SpeechView> spvs, int centerX, int centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.spvs = spvs;
        this.startPositions = new ArrayList<>();
        for (SpeechView spv : spvs) {
            startPositions.add(new Point(spv.getX(), spv.getY()));
        }
    }

    @Override
    public void start() {
        isPlaying = true;
        startTimestamp = System.currentTimeMillis();
    }

    @Override
    public void stop() {
        isPlaying = false;
    }

    @Override
    public void animate() {
        if (!isPlaying) return;

        long currTimestamp = System.currentTimeMillis();
        if (currTimestamp - startTimestamp > 2000) {
            startTimestamp += 2000;
        }

        long delta = currTimestamp - startTimestamp;

        float interpolatedTime = (float) delta / 2000;

        float angle = interpolatedTime * ROTATION_DEGREES;

        int i = 0;
        for (SpeechView spv : spvs) {
            float finalAngle = angle;
            if (i > 0 && delta > 1000) {
                finalAngle += decelerate(delta, spvs.size() - i);
            } else if (i > 0) {
                finalAngle += accelerate(delta, spvs.size() - i);
            }
            rotate(spv, finalAngle, startPositions.get(i));
            i++;
        }
    }

    private float decelerate(long delta, int scale) {
        long accelerationDelta = delta - 1000;
        AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
        float interpolatedTime = interpolator.getInterpolation((float) accelerationDelta / 1000);
        float decelerationAngle = -interpolatedTime * (ACCELERATION_ROTATION_DEGREES * scale);
        return ACCELERATION_ROTATION_DEGREES * scale + decelerationAngle;
    }

    private float accelerate(long delta, int scale) {
        long accelerationDelta = delta;
        AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
        float interpolatedTime = interpolator.getInterpolation((float) accelerationDelta / 1000);
        float accelerationAngle = interpolatedTime * (ACCELERATION_ROTATION_DEGREES * scale);
        return accelerationAngle;
    }

    private void rotate(SpeechView spv, double degrees, Point startPosition) {

        double angle = Math.toRadians(degrees);

        int x = centerX + (int) ((startPosition.x - centerX) * Math.cos(angle) -
                (startPosition.y - centerY) * Math.sin(angle));

        int y = centerY + (int) ((startPosition.x - centerX) * Math.sin(angle) +
                (startPosition.y - centerY) * Math.cos(angle));

        spv.setX(x);
        spv.setY(y);
        spv.update();
    }
}
