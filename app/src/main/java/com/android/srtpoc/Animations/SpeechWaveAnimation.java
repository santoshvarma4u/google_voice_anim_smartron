package com.android.srtpoc.Animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.android.srtpoc.Models.SpeechView;
import com.android.srtpoc.PPSAnim;

import java.util.Random;

/**
 * Created by SantoshT on 9/14/2017.
 */

public class SpeechWaveAnimation implements PPSAnim {

    private static final long UP_ANIMATION_TIME = 130;
    private static final long DOWN_ANIMATION_TIME = 500;
    final private SpeechView spv;
    private float fromHeightPart;
    private float toHeightPart;
    private long startTimestamp;
    private boolean isPlaying;
    private boolean isUpAnimation;

    public SpeechWaveAnimation(SpeechView spv) {
        this.spv = spv;
    }

    @Override
    public void start() {
        isPlaying = true;
    }

    @Override
    public void stop() {
        isPlaying = false;
    }

    @Override
    public void animate() {
        if (isPlaying) {
            update();
        }
    }

    public void onRmsChanged(float rmsdB) {
        float newHeightPart;

        if (rmsdB < 2f) {
            newHeightPart = 0.2f;
        } else if (rmsdB >= 2f && rmsdB <= 5.5f) {
            newHeightPart = 0.3f + new Random().nextFloat();
            if (newHeightPart > 0.6f) newHeightPart = 0.6f;
        } else {
            newHeightPart = 0.7f + new Random().nextFloat();
            if (newHeightPart > 1f) newHeightPart = 1f;

        }

        if (newHeightIsSmallerCurrent(newHeightPart)) {
            return;
        }

        fromHeightPart = (float) spv.getHeight() / spv.getMaxHeight();
        toHeightPart = newHeightPart;

        startTimestamp = System.currentTimeMillis();
        isUpAnimation = true;
        isPlaying = true;
    }

    private boolean newHeightIsSmallerCurrent(float newHeightPart) {
        return (float) spv.getHeight() / spv.getMaxHeight() > newHeightPart;
    }

    private void update() {

        long currTimestamp = System.currentTimeMillis();
        long delta = currTimestamp - startTimestamp;

        if (isUpAnimation) {
            animateUp(delta);
        } else {
            animateDown(delta);
        }
    }

    private void animateUp(long delta) {
        boolean finished = false;
        int minHeight = (int) (fromHeightPart * spv.getMaxHeight());
        int toHeight = (int) (spv.getMaxHeight() * toHeightPart);

        float timePart = (float) delta / UP_ANIMATION_TIME;

        AccelerateInterpolator interpolator = new AccelerateInterpolator();
        int height = minHeight + (int) (interpolator.getInterpolation(timePart) * (toHeight - minHeight));

        if (height < spv.getHeight()) {
            return;
        }

        if (height >= toHeight) {
            height = toHeight;
            finished = true;
        }

        spv.setHeight(height);
        spv.update();

        if (finished) {
            isUpAnimation = false;
            startTimestamp = System.currentTimeMillis();
        }
    }

    private void animateDown(long delta) {
        int minHeight = spv.getRadius() * 2;
        int fromHeight = (int) (spv.getMaxHeight() * toHeightPart);

        float timePart = (float) delta / DOWN_ANIMATION_TIME;

        DecelerateInterpolator interpolator = new DecelerateInterpolator();
        int height = minHeight + (int) ((1f - interpolator.getInterpolation(timePart)) * (fromHeight - minHeight));

        if (height > spv.getHeight()) {
            return;
        }

        if (height <= minHeight) {
            finish();
            return;
        }

        spv.setHeight(height);
        spv.update();
    }

    private void finish() {
        spv.setHeight(spv.getRadius() * 2);
        spv.update();
        isPlaying = false;
    }
}
